from . import db


class Task(db.Model):
    """
    Class Task corresponds to task table in the database and fields corresponds
    to the columns.
    """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    description = db.Column(db.String(200))
    priority = db.Column(db.String(10))
    complete = db.Column(db.Boolean)
    list_id = db.Column(db.Integer, db.ForeignKey('list.id'))
