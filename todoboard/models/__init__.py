from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

from .list import List
from .task import Task

__all__ = ('User', 'Task')
