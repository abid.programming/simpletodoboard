from flask import render_template, Blueprint, jsonify, request
from todoboard.models import Task
from todoboard import db

task = Blueprint('task', __name__)


@task.route('/task/delete/<int:task_id>', methods=['POST'])
def deleteTask(task_id):
    '''
    This route queries the database for task_element by task_id and deletes it.
    If the task_id is not found it returns a 404.
    '''
    task = Task.query.get_or_404(task_id)
    list_id = task.list_id
    db.session.delete(task)
    db.session.commit()
    return jsonify({"list_id": list_id}), 200


@task.route('/task/complete/<int:task_id>', methods=['POST'])
def completeTask(task_id):
    '''
    This route queries the database for task_element by task_id and
    sets task.complete to True.
    If the task_id is not found it returns a 404.
    The route returns the jsonified data containing
    id, name, description, complete, priority and it's corresponding list_id
    '''
    task = Task.query.get_or_404(task_id)
    complete = task.complete = True
    name = task.name
    if len(name) > 25:
        return 413
    description = task.description
    priority = task.priority
    list_id = task.list_id
    db.session.add(task)
    db.session.commit()
    return jsonify({
        "id": task.id,
        "name": name,
        "description": description,
        "complete": complete,
        "priority": priority,
        "list_id": list_id
    }), 200


@task.route('/list/<int:list_id>/task/add/', methods=['POST'])
def addTask(list_id):
    '''
    This route gets the list_id and json data about task and inserts it to the database
    The route returns the jsonified data containing
    id, name, description, complete, priority and it's corresponding list_id
    '''
    data = request.get_json()
    name = data['name']
    if len(name) > 25:
        return 'File Too Large',413
    description = data['description']
    priority = data['priority']
    task = Task(name=name,
                description=description,
                priority=priority,
                list_id=list_id,
                complete=False)
    db.session.add(task)
    db.session.commit()
    return jsonify({
        "id": task.id,
        "name": name,
        "description": description,
        "complete": False,
        "priority": priority,
        "list_id": list_id
    }), 200
