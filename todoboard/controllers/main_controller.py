from flask import render_template, jsonify, Blueprint
from todoboard.models import List, Task
from todoboard.forms import ListForm, TaskForm

main_route = Blueprint('main_route', __name__)


@main_route.route("/", methods=['GET', 'POST'])
def home():
    '''
    This route queries lists and tasks do display them on the homepage and
    sends the list form and task form to board.html to get rendered.
    '''
    lists = List.query.all()
    tasks = Task.query.all()
    listform = ListForm()
    taskform = TaskForm()
    return render_template('board.html',
                           lists=lists,
                           tasks=tasks,
                           listform=listform,
                           taskform=taskform)
