"""
forms.py contains all forms and there respective validations.
"""

from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, BooleanField, SelectField
from wtforms.validators import DataRequired, ValidationError
from wtforms.widgets import TextArea


def length(min=-1, max=-1):
    '''
    length function is used as a validator for the flask forms.

    function checks whether the length of field is in valid range
    provided by the user and raises a ValidationError.

    Parameters:
    min (int): minimum no of length allowed for the field
    max (int): maximum no of length allowed for the field
    '''

    message = 'Must be between %d and %d characters long.' % (min, max)

    def _length(form, field):
        l = field.data and len(field.data) or 0
        if l < min or max != -1 and l > max:
            raise ValidationError(message)

    return _length


class ListForm(FlaskForm):
    '''
    Class ListForm contains the fields and there respective validators
    which are used in List form.
    '''
    name = StringField('Enter the name',
                       validators=[DataRequired(),
                                   length(min=3, max=15)])
    description = StringField('Enter a description',
                              validators=[length(max=200)])
    submit = SubmitField('Add List')


class TaskForm(FlaskForm):
    '''
    Class TaskForm contains the fields and there respective validators
    which are used in Task form.
    '''
    name = StringField('Enter the name',
                       validators=[DataRequired(),
                                   length(min=3, max=15)])
    description = StringField('Enter a description',
                              validators=[length(max=200)])
    priority = SelectField('Priority',
                           choices=[('low', 'Low'), ('medium', 'Medium'),
                                    ('high', 'High')])
    submit = SubmitField('Add Task')
